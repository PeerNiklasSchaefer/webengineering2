### Web-Engineering 2

This is a fork of the [Biletado](https://gitlab.com/biletado)-repository with a modified compose.yml-file to fit our Project. \
It contains the Code for the final exam in Web-Engineering 2. \
The API can be found in  the [APIProgress](https://gitlab.com/PeerNiklasSchaefer/webengineering2/-/tree/APIProgress)-branch. 

Examinees: Patrick Ell, Marcel Reith, Nico Rahm, Peer Niklas Schäfer \
Auditor: Matthias Blümel \
Date of Submission: 31. May 2022 

The environment is set up by cloning this repository and running _docker-compose up -d_.
